/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.employee;

/**
 *
 * @author jayvy
 */
public class PayrollSimulation {
    
	public static void main(String[] args) {
		
		Employee e1 = new Employee (12.3, 6.3, "James");
		Manager m1 = new Manager(20.5, 6.3, "Max", 32.56);
		
		System.out.println (e1.calculatePay());
		System.out.println(m1.calculatePay());

	}

}

